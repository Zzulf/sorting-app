package com.zulf.sorting_app;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

/**
 * Unit test for simple App.
 */
@RunWith(Parameterized.class)
public class AppTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;
	private final PrintStream originalErr = System.err;

	@Before
	public void setUpStreams() {
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
	}

	@After
	public void restoreStreams() {
		System.setOut(originalOut);
		System.setErr(originalErr);
	}

	@Parameters()
	public static Iterable<String[][]> data() {
		return Arrays.asList(new String[][][] { { new String[] { "1" }, new String[] { "1" } },
				{ new String[] { "2", "1" }, new String[] { "1", "2" } },
				{ new String[] { "9", "8", "7", "6", "5", "4", "3", "2", "1", "0" },
						new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"} },
				{ new String[] { "" }, new String[] { "" } },
				{ new String[] { "", "" }, new String[] { "", "" } } });
	}

	private String[] input;
	private String[] expected;

	public AppTest(String[] input, String[] expected) {
		this.input = input;
		this.expected = expected;
	}

	@Test
	public void testSortingCases() {
		App.main(input);
		assertEquals(String.join(" ", expected).trim(), outContent.toString().trim());
	}
}

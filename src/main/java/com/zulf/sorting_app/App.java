package com.zulf.sorting_app;

import java.util.Arrays;

/**
 * It is a small Java application that takes up to ten command-line arguments as
 * integer values, sorts them in the ascending order, and then prints them into
 * standard output.
 *
 */
public class App {
	private static final String INTEGER_PATTERN = "^-?\\d+$";

	public static void main(String[] args) {
		Arrays.sort(args);
		for (String item : args) {
			if (item.matches(INTEGER_PATTERN)) {
				System.out.print(item + " ");
			}
		}
	}
}
